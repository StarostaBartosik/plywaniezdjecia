package com.example.maciek.photoapp.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.maciek.photoapp.R
import kotlinx.android.synthetic.main.fragment_data.*


class DataFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        fragment_date.text = arguments!!.getString("date")
        fragment_title.text = arguments!!.getString("title")
        fragment_tags.text = arguments!!.getString("tags")

    }

    companion object {

        @JvmStatic
        fun newInstance(title: String, date: String, tags: String) =
                DataFragment().apply {
                    arguments = Bundle().apply {
                        putString("title", title)
                        putString("date", date)
                        putString("tags", tags)
                    }
                }
    }
}
