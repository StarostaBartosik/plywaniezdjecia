package com.example.maciek.photoapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import com.example.maciek.photoapp.fragments.DataFragment
import com.example.maciek.photoapp.fragments.FullPhotoFragment
import com.example.maciek.photoapp.fragments.SimilarPhotosFragment
import kotlinx.android.synthetic.main.activity_show.*

class ShowActivity : AppCompatActivity() {


    private var isFullPhotoLoaded: Boolean = true
    private var fullPhotoFragment: FullPhotoFragment? = null
    private var dataFragment: DataFragment? = null
    private var similarPhotosFragment: SimilarPhotosFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show)
        setUpFragments()
        changeButton.setOnClickListener {
            changeFragment()
        }


    }


    private fun setUpFragments() {
        fullPhotoFragment = FullPhotoFragment.newInstance(intent.getStringExtra("url"))
        dataFragment = DataFragment.newInstance(intent.getStringExtra("title"), intent.getStringExtra("date"), intent.getStringExtra("tags"))
        similarPhotosFragment = SimilarPhotosFragment.newInstance(intent.getStringArrayListExtra("similarUrls"))

        transaction()
                .replace(R.id.container, fullPhotoFragment)
                .replace(R.id.upLayout, dataFragment)
                .replace(R.id.downLayout, similarPhotosFragment)
                .hide(dataFragment)
                .hide(similarPhotosFragment)
                .commit()
    }

    @SuppressLint("CommitTransaction")
    private fun transaction(): FragmentTransaction {
        return supportFragmentManager
                .beginTransaction()

    }

    private fun loadTwoFragmens() {
        transaction()
                .hide(fullPhotoFragment)
                .show(dataFragment)
                .show(similarPhotosFragment)
                .commit()
    }

    private fun loadFullPhotoFragment() {
        transaction()
                .hide(dataFragment)
                .hide(similarPhotosFragment)
                .show(fullPhotoFragment)
                .commit()

    }

    private fun changeFragment() {
        isFullPhotoLoaded = if (isFullPhotoLoaded) {
            loadTwoFragmens()
            false
        } else {
            loadFullPhotoFragment()
            true
        }
    }


}
