package com.example.maciek.photoapp.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.maciek.photoapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_similar_photos.*


class SimilarPhotosFragment : Fragment() {

    private var photosArray: ArrayList<ImageView> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_similar_photos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        putImagesIntoArray()
        val similar = arguments!!.getStringArrayList("similarUrls")
        for (index in 0 until similar.size) {
            Picasso.get().load(similar[index]).into(photosArray[index])
        }
    }


    fun putImagesIntoArray() {
        photosArray.add(fragment_imageView1)
        photosArray.add(fragment_imageView2)
        photosArray.add(fragment_imageView3)
        photosArray.add(fragment_imageView4)
        photosArray.add(fragment_imageView5)
        photosArray.add(fragment_imageView6)

    }


    companion object {

        @JvmStatic
        fun newInstance(similarUrls: ArrayList<String>) =
                SimilarPhotosFragment().apply {
                    arguments = Bundle().apply {
                        putStringArrayList("similarUrls", similarUrls)
                    }
                }
    }
}
