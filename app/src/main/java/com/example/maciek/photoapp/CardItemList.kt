package com.example.maciek.photoapp


class CardItemList {

    private val list: ArrayList<CardItem> = ArrayList()

    init {

        list.add(CardItem("http://snaa.pwr.edu.pl/images/pwr.png", "Logo PWR", "3/12/2018", arrayOf("nuda", "poprawka", "więcej nudy")))
        list.add(CardItem("https://www.safiras.de/pl/system/images/prod_safira_gruen_links.png", "Safiras", "13/06/1998", arrayOf("super", "extra", "smoki")))

    }


    fun getList(): ArrayList<CardItem> {

        return list
    }

    fun addToList(item: CardItem) {
        list.add(item)
    }

    fun getElement(position: Int): CardItem {
        return list[position]
    }

    fun addTagToElement(index: Int, tag: String) {
        var exits = false
        list[index].tags.forEach {
            if (it == tag) {
                exits = true
            }
        }
        if (!exits) {
            list[index].tags += tag
        }

    }
}






