package com.example.maciek.photoapp

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add.*
import java.util.*


class AddActivity : AppCompatActivity() {

    private var mDateSetListener: DatePickerDialog.OnDateSetListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        setUpDataPicker()
    }


    fun addNewCard(view: View) {

        if (url.text.toString() != "" && photoTitle.text.toString() != "" && date.text.toString() != "" && tags.text.toString() != "") {
            val intent = Intent(this, MainActivity::class.java).apply {
                putExtra("url", url.text.toString())
                putExtra("title", photoTitle.text.toString())
                putExtra("date", date.text.toString())
                putExtra("tags", tags.text.toString())
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        } else
            Toast.makeText(this, "Please give me all information", Toast.LENGTH_SHORT).show()
    }

    private fun setUpDataPicker() {
        date.setOnClickListener {
            val cal = Calendar.getInstance()
            val year = cal.get(Calendar.YEAR)
            val month = cal.get(Calendar.MONTH)
            val day = cal.get(Calendar.DAY_OF_MONTH)

            val dialog = DatePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    mDateSetListener,
                    year, month, day)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }
        mDateSetListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
            var newMonth = month
            newMonth += 1

            val dateString = "$day/$newMonth/$year"
            date.text = dateString
        }
    }

}
