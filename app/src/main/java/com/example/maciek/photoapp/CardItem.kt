package com.example.maciek.photoapp


data class CardItem(var image: String, var title: String, var date: String, var tags: Array<String>)
