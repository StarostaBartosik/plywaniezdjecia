package com.example.maciek.photoapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val NEW_CARD = 1337
    private val list: CardItemList = CardItemList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_photo -> {
                val i = Intent(this, AddActivity::class.java)
                startActivityForResult(i, NEW_CARD)

            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NEW_CARD) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val tags = data.getStringExtra("tags").split(",").toTypedArray()
                    list.addToList(CardItem(data.getStringExtra("url"), data.getStringExtra("title"), data.getStringExtra("date"), tags))
                    RecyclerView.adapter.notifyDataSetChanged()


                }
            }
        }
    }

    private fun setUpRecyclerView() {
        val adapter = CardItemAdapter(list, this)

        RecyclerView.layoutManager = LinearLayoutManager(this)
        RecyclerView.adapter = adapter
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(adapter))
        itemTouchHelper.attachToRecyclerView(RecyclerView)
    }
}
