package com.example.maciek.photoapp.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.maciek.photoapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_full_photo.*


class FullPhotoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_full_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Picasso.get().load(arguments!!.getString("url")).into(imageFull)
    }


    companion object {

        @JvmStatic
        fun newInstance(url: String) =
                FullPhotoFragment().apply {
                    arguments = Bundle().apply {
                        putString("url", url)
                    }
                }
    }
}
