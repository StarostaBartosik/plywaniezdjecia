package com.example.maciek.photoapp

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.card_item.view.*


class CardItemAdapter(private val cardItems: CardItemList, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    private val items = cardItems.getList()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        updateWithUrl(items[position].image, holder, position)
        holder.title!!.text = makeStringShorter(items[position].title)
        holder.date!!.text = items[position].date
        holder.tags!!.text = tagsToString(items[position].tags)


        holder.itemView.setOnClickListener {
            val intent = Intent(context, ShowActivity::class.java)
            intent.putExtra("url", items[position].image)
            intent.putExtra("title", items[position].title)
            intent.putExtra("date", items[position].date)
            intent.putExtra("tags", cardItems.getList()[position].tags.joinToString(", ") { it })
            intent.putExtra("similarUrls", findSimilarPhotos(position))
            context.startActivity(intent)
        }
    }

    fun processImageTagging(bitmap: Bitmap, holder: ViewHolder, position: Int) {
        val visionImg = FirebaseVisionImage.fromBitmap(bitmap)
        val labeler: FirebaseVisionImageLabeler = FirebaseVision.getInstance().onDeviceImageLabeler
        labeler.processImage(visionImg)
                .addOnSuccessListener { tagsFirebase ->
                    tagsFirebase.forEach { cardItems.addTagToElement(position, it.text) }
                    holder.tags!!.text = tagsFirebase.joinToString(", ") { it.text }
                }
                .addOnFailureListener { ex ->
                    Log.wtf("WTF", ex)
                }


    }

    private fun updateWithUrl(url: String, holder: ViewHolder, position: Int) {

        val target: Target = object : Target {
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom) {
                bitmap?.apply {
                    processImageTagging(this, holder, position)
                    holder.myImageView.setImageBitmap(this)
                }
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        holder.myImageView.tag = target
        Picasso.get().load(url).into(target)
    }


    private fun findSimilarPhotos(index: Int): ArrayList<String> {
        val chosenItem = cardItems.getElement(index)
        var similarUrlsArray: ArrayList<String> = ArrayList()

        cardItems.getList().forEach { item ->
            chosenItem.tags.forEach {
                if (item.tags.find { currentTag ->
                            currentTag == it
                        } != null) {
                    similarUrlsArray.add(item.image)

                }
            }

        }
        similarUrlsArray = ArrayList(similarUrlsArray.distinct().filter { chosenItem.image != it })

        return similarUrlsArray

    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.card_item, parent, false))
    }


    private fun tagsToString(array: Array<String>): String {
        var tags = ""
        if (tags.length > 2) {
            for (i in 0..2) {
                tags += array[i] + ", "
            }
        } else {
            array.forEach { tags += "$it, " }
        }
        return tags.substring(0, tags.length - 2)
    }


    private fun makeStringShorter(string: String): String {
        return if (string.length > 20) {
            string.substring(0, 20) + "..."
        } else
            string
    }

    fun deleteItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }


}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val myImageView: ImageView = view.photo
    val title: TextView? = view.title
    val date: TextView? = view.date
    val tags: TextView? = view.tags

}
